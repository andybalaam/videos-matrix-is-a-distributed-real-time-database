# videos-matrix-is-a-distributed-real-time-database

Slides for "Matrix is a Distributed Real-time Database", a talk for
ACCU 2022 Conference.

View the slides as a presentation here:
[Matrix is a Distributed Real-time Database](http://artificialworlds.net/presentations/matrix-is-a-distributed-real-time-database/matrix-is-a-distributed-real-time-database.html)

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
    <img
        alt="Creative Commons License"
        style="border-width:0"
        src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"
    />
</a>
<br />This work is licensed under a
    <a
        rel="license"
        href="http://creativecommons.org/licenses/by-sa/4.0/"
    >Creative Commons Attribution-ShareAlike 4.0 International License</a>.

