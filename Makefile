
NAME := matrix-is-a-distributed-real-time-database

all:
	echo "try 'make upload'"

upload:
	rsync -r \
		--exclude .git \
		./ \
		dreamhost:artificialworlds.net/presentations/${NAME}/

zip:
	mkdir -p pkg
	- rm -rf "pkg/${NAME}"
	mkdir -p "pkg/${NAME}"
	cp -r *.html *.css *.js LICENSE images/ moreinfo/ "pkg/${NAME}/"
	cd pkg && zip -r "${NAME}.zip" "${NAME}"
